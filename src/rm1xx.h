/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */
#ifndef RM1XX_H
#define RM1XX_H

#ifdef __cplusplus
extern "C" {
#endif


#define RX_PIN_NUMBER  22
#define TX_PIN_NUMBER  21
#define CTS_PIN_NUMBER 23
#define RTS_PIN_NUMBER 24
#define HWFC           true

#define SPIS_MISO_PIN  10    // SPI MISO signal.
#define SPIS_CSN_PIN   8    // SPI CSN signal.
#define SPIS_MOSI_PIN  9    // SPI MOSI signal.
#define SPIS_SCK_PIN   11    // SPI SCK signal.

#ifdef __cplusplus
}
#endif

#endif // RM1XX_H
