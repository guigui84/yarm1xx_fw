# yarm1xx_fw

An wannabe open source firmware for the moduleRM1XX (RM186 and RM191) from Laird. For more information, have a look at the [wiki](https://gitlab.com/guigui84/yarm1xx_fw/wikis/home).

## Disclaimer

- All the information is provided as is and is not guaranteed.
- I am not responsible if you [burn/brick/whatever you do bad to] your module.
- This is an experimental and is not ready for industrial use.
- Using a custom firmware will break the module's FCC/RED certifications.

## Gear

This project is developed using the following setup

#### OS:

**Ubuntu** 18.04

#### Programming interface:

**Broken STM32 Nucleo L152RE** (with SWD straps removed)

#### Compiler:

**gcc** (provided in nRF5 SDK)

#### flashing software:

**openocd** v0.10.0 (default with Ubuntu 18.04)

## Getting started

- Download and extract Nordic SDK v12.10 for nRF5 from [https://www.nordicsemi.com/Software-and-Tools/Software/nRF5-SDK/Download](https://www.nordicsemi.com/Software-and-Tools/Software/nRF5-SDK/Download)

- Edit ***Makefile.project*** to set the right path for the SDK

- Connect your RM1XX to the computer through UART and SWD

- Open a connection on your UART interface:

  `picocom -b 115200 /dev/ttyACM0`

- Build the firmware:

  `make`

- Flash the firmware to the RM1XX:

  `make flash`

- You should see the following on your serial console:

  `~°^°~ Congratulations, you just started yarm1xx_fw ~°^°~`
